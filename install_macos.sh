brew install python3

python3 -m venv ./env
source env/bin/activate

pip install --upgrade pip
pip install psycopg2==2.7.7
pip install ppdli

#=========== server install============
sudo mkdir /Library/Application\ Support/pytaskmanager
sudo chown -R $(whoami): /Library/Application\ Support/pytaskmanager

ppserver start -c server/test.yaml &

ppserver import -c server/test.yaml server/example_fixtures.yaml